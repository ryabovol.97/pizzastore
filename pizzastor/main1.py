from objects.pizza_store1 import PizzaStore
from objects.llist_pizzas import print_greetings, client_request, print_wrong_choice


def main(pizza_stor):
    while True:
        print_greetings()
        match client_request():
            case '0':
                break
            case '1':
                pizza_stor.all_pizzas()
            case '2':
                pizza_stor.list_of_pizzas_filtered_by_customer_amount()
            case '3':
                pizza_stor.random_check()
            case _:
                print_wrong_choice()
    print('Bye')


if __name__ == '__main__':
    pizza_store = PizzaStore()
    main(pizza_store)
