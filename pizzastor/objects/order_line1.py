from objects.pizza1 import Pizza


class OrderLine:

    def __init__(self, pizza: Pizza, quantity):
        self.pizza = pizza
        self.quantity = quantity
        self.sum = float(pizza.price) * float(quantity)

    def __str__(self):
        return f'Pizza: {self.pizza}' \
               f'Amount: {self.quantity}' \
               f'Summa: {self.sum}'
