predictions = (
    'очень скоро у тебя будут новые друзья.',
    'Ваши надежды и планы сбудутся.',
    'Готовьтесь к романтическим приключениям.',
    'В этом месяце ночная жизнь для вас.',
    'Вам пора отдохнуть.',
    'Вам предлагается мечта всей жизни.',
    'Вас ждет приятный сюрприз.',
    'Ваши надежды и планы сбудутся.',
    'Вас ждут много сюрпризов!',
    'В доме вкусная еда будет у тебя всегда.'
)


def print_greetings():
    print('Hi!\n0 - Exit'
          '\n1 - Show all list of pizzas'
          '\n2 - Filtered list of pizzas by the amount you specify?'
          '\n3 - Random check')


def client_request():
    return input('Your choice: ')


def print_wrong_choice():
    print('Wrong choice! Try again\n')
