from objects.llist_pizzas import predictions
import random


class Order:
    total_sum = 0

    def __init__(self, check_number, date_now):
        self.check_number = check_number
        self.date_now = date_now

    def shapka(self):
        if self.check_number >= 0:
            self.check_number += 1
        print(f'{" " * 60}{" " * 15}Wellcome'
              f'\n{" " * 60}{" " * 12}Check number: {self.check_number}'
              f'\n{" " * 60}{"*" * 39}')

    def __str__(self):
        txt = f'\n{" " * 60}{"*" * 39}'
        txt += f'\n{" " * 60}Time/Date:        {self.date_now.now().strftime("%H:%M:%S / %d-%m-%Y")}'
        txt += f'\n{" " * 60}Предсказание для вас:'
        txt += f'\n{" " * 60}{" ".join(random.sample(predictions, 1))}'
        txt += f'\n{" " * 60}{"#" * 39}'
        txt += f'\n{" " * 60}           *** Good bay ***\n'
        return txt
