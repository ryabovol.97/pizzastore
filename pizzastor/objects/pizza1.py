class Pizza:

    def __init__(self, idx, name, price, description):
        self.idx = idx
        self.name = name
        self.price = price
        self.description = description

    def __str__(self):
        return f'Pizza index = {self.idx}\n' \
               f'Pizza name = {self.name}' \
               f'Pizza price = {self.price}' \
               f'Pizza description = {self.description}'
