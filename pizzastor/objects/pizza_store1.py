from objects.pizza1 import Pizza
from objects.order1 import Order
from objects.order_line1 import OrderLine
from exception.my_exception import MyException
from random import randint, sample
from datetime import datetime
from Singleton.SingletonMeta import SingletonMeta
import csv
import os


class PizzaStore(metaclass=SingletonMeta):

    def __init__(self):
        self.pizzas_list = self.read_pizza_from_file(os.path.join('constants', 'pizzas_file.csv'))
        self.order_class = Order(0, datetime.now())

    def read_pizza_from_file(self, filename):
        pizzas = []
        with open(filename, 'rt') as f:
            reader = csv.DictReader(f)
            for row in reader:
                pizzas.append(Pizza(row['idx'], row['name'], row['price'], row['description']))
        return pizzas

    def add_new_pizza(self, new_pizza):
        with open(os.path.join('constants', 'pizzas_file.csv'), 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(tuple(new_pizza))

    def all_pizzas(self):
        print(f'\n{" " * 61}***ALL PIZZAS PIZZERIAS***\n'
              f'\n{" " * 45}{"*" * 58}')
        for idx, item in enumerate(self.pizzas_list, start=1):
            print(f'{" " * 45}{idx}.Type of pizza: {item.name}'
                  f'\n{" " * 45}  Ingredients: {"+".join(item.description.split("+")[:3])}'
                  f'\n{" " * 45}  {"+".join(item.description.split("+")[3:])}'
                  f'\n{" " * 45}  price:{"." * 40}{"%.2f" % int(item.price)} UAH')
        print(f'{" " * 45}{"*" * 59}')

    def list_of_pizzas_filtered_by_customer_amount(self):
        budget = int(input('\nSpecify the amount in the range 150 - 250 UAH,'
                           '\nand we will select your personal list of pizzas: '))
        try:
            if budget < 150:
                raise MyException('Нищебродам не место в моей пиццерии!')
            elif budget > 250:
                raise MyException('Боюсь у меня нет настолько дорогой пиццы для тебя.')
        except MyException as e:
            print('\nОшибочка!', e, "\n")
        else:
            filter_by_sum_of_pizzas = [item for item in self.pizzas_list if int(item.price) <= budget]
            print(f'\n{" " * 61}***SPECIAL OFFER FOR YOU***\n'
                  f'\n{" " * 45}{"*" * 58}')
            for idx, item in enumerate(filter_by_sum_of_pizzas, start=1):
                print(f'{" " * 45}{idx}.Type of pizza: {item.name}'
                      f'\n{" " * 45}  Ingredients: {"+".join(item.description.split("+")[:3])}'
                      f'\n{" " * 45}  {"+".join(item.description.split("+")[3:])}'
                      f'\n{" " * 45}  price:{"." * 40}{"%.2f" % int(item.price)} UAH')
            print(f'{" " * 45}{"*" * 59}')

    def random_check(self):
        total_sum = 0
        self.order_class.shapka()
        for idx, item in enumerate(sample(self.pizzas_list, randint(1, 5)), start=1):
            order_line = OrderLine(item, randint(1, 3))
            total_sum += order_line.sum
            print(f'{" " * 60}{idx}.Type of pizza: "{item.name}"'
                  f'\n{" " * 60}  Ingredients: {"+".join(item.description.split("+")[:2])}'
                  f'\n{" " * 60}  {"+".join(item.description.split("+")[2:])}'
                  f'\n{" " * 60}  {item.price}UAH x {order_line.quantity} '
                  f'\n{" " * 60}  price:{"." * 21}{"%.2f" % order_line.sum} UAH'
                  f"\n{' ' * 60}{'=' * 39}")
        print(f"{' ' * 60}Total{' ' * 22} {'%.2f' % total_sum} UAH".rjust(99), self.order_class)

    def __str__(self):
        return f'PizzaStore: pizzas= {self.pizzas_list}'
